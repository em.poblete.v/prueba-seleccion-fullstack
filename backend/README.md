## Back End Tactech Emilio Poblete Valenzuela
### Aplicacion
- [Backend](https://gotapimongotactech.herokuapp.com/ "Backend") - Rest API hosteada en Heroku 
- [MongoDB](https://www.mongodb.com/cloud "MongoDB") - Base de datos en nube

## Solucion
Desarrollo de REST API que obtiene personajes de API GOT, los inserta en su base de datos para luego mostrar esta informacion.
## Tecnologías
- [Node.js](https://nodejs.org/en/ "Node.js")
- [Express](https://expressjs.com/ "Express")
- [Mongoose](https://mongoosejs.com/ "Express")
- [MongoDB](https://www.mongodb.com/cloud "MongoDB")
## Configuracion
- git clone

- cd backend

-  npm install

-  npm start

Para el correcto funcionamiento de la API REST, es necesario configurar el endpoint de la base de datos MongoDB a consumir en el archivo config.json.

## Rutas
- /characters
- /characters/:id
