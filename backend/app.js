const app = require('./src/Server/express');
const { connectDB } = require('./src/Config/database');
const config = require('./src/Config/Config.json');
const { getGotChars } = require('./src/Helper/service');
const port = process.env.PORT || config.port;

connectDB();
getGotChars();
app.listen(port, () => console.log(`Server listening in port: ${port}`));


module.exports = app;