const mongoose = require('mongoose');
const config = require('./Config.json');

let dbURL = process.env.MONGODB || config.dbURL

exports.connectDB = async() =>{
    const options = {
        useNewUrlParser: true,
        useUnifiedTopology: true,
        useFindAndModify: false,
        keepAlive: 30000,
        connectTimeoutMS: 20000,
    }

    try{
        await mongoose.connect(dbURL,options);
        console.log('Connection Established');
                   
    }
    catch(err){
        console.log(err);
    } 
}
