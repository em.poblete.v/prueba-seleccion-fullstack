const Characters = require('../Models/characters'); 

exports.getAllCharacters = async( req, res)=>{
    try{
        let characters = await Characters.find({})
        return res.status(200).json({ data:characters });
    }
    catch(error){
        console.log(error);
        return res.status(500).send({ error:error.message });
    }

}

exports.getCharacterId = async( req, res )=>{
    const { id } = req.params
    try{

        let character = await Characters.findById( {_id : id} );
        if( !character ){
            return res.status(404).json({ data:`Character doesn't exist!` });
        }
        else{
            return res.status(200).json({ data:character });
        }

    }
    catch( error ){
        console.log( error );
        return res.status(500).send({ error:'El id del personaje debe ser de 12 caracteres' });
    }
}