const axios = require('axios');
const Characters = require('../Models/characters');
const config = require('../Config/Config.json');

exports.getGotChars = () => {
    try{
      Characters.countDocuments().then(async (count) => {
            if(count == 0){
                let { data } = await axios({
                    url:config.endpoint,
                    method:'GET'
                });
                let newCharacters = data.map((char) => new Characters(char));
                Characters.create(newCharacters);
              }
      });  
    }
    catch(error){
        console.log(error);
    }
}
 