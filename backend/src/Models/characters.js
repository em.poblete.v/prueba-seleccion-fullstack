const { Schema, model } = require('mongoose');


const Characters = new Schema({
    
    image:{type: String },
    
    name:{type:String},
    
    gender:{type:String},
    
    slug:{type:String},
    
    pagerank: {type: Object },
    
    house:{type:String},
    
    books:{type: Array},

    titles:{type: Array},
    


});

module.exports = model('characters',Characters)