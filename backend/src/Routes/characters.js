const express = require('express');
const router = express.Router();
const { getAllCharacters, getCharacterId } = require('../Controller/characters');

router.get('/characters', getAllCharacters);
router.get('/characters/:id', getCharacterId);

module.exports = router;