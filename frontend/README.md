## Front-End Tactech Emilio Poblete Valenzuela
### Aplicacion
- [Pagina web](https://gotcharinfo.web.app/ "Pagina web") - Pagina hosteada en Firebase 

## Solucion
Desarrollo de REST API que obtiene personajes de API GOT, los inserta en su base de datos para luego mostrar esta informacion.
## Tecnologías
- [ReactJS](https://reactjs.org/ "ReactJS")
- [Express](https://expressjs.com/ "Express")
- [Material-UI](https://material-ui.com/ "Material-UI")
- [Material-Table](https://material-table.com/#/ "Material-Table")
- [Material-Table](https://material-table.com/#/ "Material-Table")
- [Axios](https://github.com/axios/axios "Axios")
- [Firebase](https://firebase.google.com/ "Firebase")

## Instalacion
- git clone

- cd frontend

-  npm install

-  npm start

## Vista Previa

### Lista de Personajes
![alt text](https://i.imgur.com/L2VLgTq.png)
### Detalles del Personaje
![alt text](https://i.imgur.com/YfcCmdk.png)

