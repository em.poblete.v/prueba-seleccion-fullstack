import './App.css';
import { TableList } from './Views/TableList' 
import { CharInfo } from './Views/CharInfo' 
import { NavBar } from './Components/NavBar/Navbar'
import { BrowserRouter as Router , Switch, Route} from 'react-router-dom'
function App() {
  return (
    <Router>
      <div className="app">
      <NavBar/>
        <Switch>
          <Route path="/" exact component = { TableList } />
          <Route path="/:id" component =  { CharInfo } />
        </Switch>
      </div>
    </Router>
    );
}

export default App;
