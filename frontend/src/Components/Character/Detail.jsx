import React, { useState, useEffect } from "react";
import { getCharacterbyId } from '../../Helper/Service';
import profilePic from '../../Images/placeholder_image.png';
import '../Character/Detail.css';


export const Detail = ( { id }) =>{

  const [data, setData] = useState([]);
  const [isLoading, setIsLoading] = useState(true);

  useEffect(() => {
    getCharacterbyId(id).then((response) => {
      setData(response.data);
      setIsLoading(false);
    });
  }, []);
  console.log(data.titles);

return(
    <>
    {!isLoading &&(
    <div>
        <div className="nameCharacter">
            <h1 className="title">{data.name}</h1>
            <h2 className="title">{data.slug}</h2>
            <img className = "profile" src = {data.image || profilePic } alt="Imagen"/>
        </div>
        <div className="infoCharacter">
             <p>{`Gender: ${data.gender}`}</p>
             <p>{`Rank: ${data.pagerank.rank}`}</p>
             <p>{`House : ${data.house}`}</p>
        </div>
        <div className ="arrayData">
            <div>
                <p className="listBooks">Books</p>
                {data.books.map((char)=>{return (<li className="listBooks">{char}</li>)})}
              </div>
            <div>
                <p className="listTitles">Titles</p>
                {data.titles.map((titles)=>{return (<li className="listTitles">{titles}</li>)})}
            </div>
        </div>
    </div>
    
    )}
    
    </>
    

  );
}