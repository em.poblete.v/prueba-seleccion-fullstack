import '../NavBar/Navbar.css'
import avatar from '../../Images/avatar.png'
export const NavBar = ()=>{
    return(
        <nav className="NavbarItems">
           <h1 className="navbar-logo">Game of Thrones<img className="avatar" src={avatar} alt="Avatar"></img></h1>
        </nav>
    );
}