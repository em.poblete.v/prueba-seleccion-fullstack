const Columns = [
    {
      title: "Name",
      field: "name",
      headerStyle:{
        fontSize:24
      }
    },
    {
      title: "House",
      field: "house",
      headerStyle:{
        fontSize:24
      }
    },

  ];
  
  export default Columns;