import React, { useState, useEffect } from "react";
import MaterialTable from "material-table";
import { getAllCharacters } from '../../Helper/Service';
import Columns from './Columns';
import tableIcons from './Icons';
import { Link } from 'react-router-dom';

export const Table = () => {
  const [data, setData] = useState([]);
  const [isLoading, setIsLoading] = useState(true);

  useEffect(() => {
    getAllCharacters().then((response) => {
      setData(response.data);
      setIsLoading(false);
    });
  }, []);
  return (
    <body>
    <MaterialTable
      icons= { tableIcons }
      isLoading={ isLoading }
      title="Characters"
      columns={ Columns }
      data={ data }
      actions={[
        rowData => ({
          icon: () => <Link to={`/${rowData._id}`}>View</Link>,
          tooltip: 'See Details',
        })
      ]}
      options={{
        actionsColumnIndex: -1,
        pageSize:10,
        rowStyle:{
          fontFamily:'Montserrat'
        }
      }}
    />
    </body>
   
  );
};