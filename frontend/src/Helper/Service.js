const axios = require('axios');
const config = require('../Config/Config.json');

export const getAllCharacters = async () =>{
    try{
        let { data } = await axios({
                url:`${config.endpoint}/characters`,
                method:'GET',
            });
            return data
    }
    catch(err){
        console.log(err.message);
    }
}


export const getCharacterbyId = async (id) =>{
    try{
        let { data } = await axios({
                url:`${config.endpoint}/characters/${id}`,
                method:'GET',
            });
            return data
    }
    catch(err){
        console.log(err.message);
    }
}