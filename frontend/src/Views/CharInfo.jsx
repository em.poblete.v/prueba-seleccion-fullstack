import { Detail } from '../Components/Character/Detail';
import { useParams } from "react-router-dom";
export const CharInfo = () =>{
    const { id } = useParams(); 
    return(
        <div>
           <Detail id= { id } ></Detail>
        </div>
    );
}